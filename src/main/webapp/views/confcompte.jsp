<!DOCTYPE html>
<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<html>
<head>
    <meta charset="UTF-8">
    <title>Confirmation de compte</title>
    <link href="https://unpkg.com/tailwindcss@^2/dist/tailwind.min.css" rel="stylesheet"/>

</head>
<body>
<div class="rounded bg-white-400 shadow-lg inline-block p-8 m-8">
    <h1 class="text-center text-blue-800">Confirmation de compte</h1>
    <div class="flex flex-col p-4">
        <span class="text-blue-400">Nom</span>
        <span class="font-light"><s:property value="compte.nom"/></span>
    </div>
    <div class="flex flex-col p-4">
        <span class="text-blue-400">Prenom</span>
        <span class="font-light"><s:property value="compte.prenom"/></span>
    </div>
    <div class="flex flex-col p-4">
        <span class="text-blue-400">Courrier</span>
        <span class="font-light"><s:property value="compte.courrier"/></span>
    </div>
    <div class="flex flex-col p-4">
        <span class="text-blue-400">Mot de passe</span>
        <span class="font-light"><s:property value="compte.password"/></span>
    </div>
    <input type="button" value="Confirmer" class="bg-blue-400 p-2 text-white bg:hover-blue-300"/>
</div>

</body>
</html>
