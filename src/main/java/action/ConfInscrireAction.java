package action;

import com.opensymphony.xwork2.ActionSupport;
import model.Laureat;

public class ConfInscrireAction extends ActionSupport {

    private Laureat laureat;

    @Override
    public String execute() throws Exception {
        return SUCCESS;
    }

    public Laureat getLaureat() {
        return laureat;
    }

    public void setLaureat(Laureat laureat) {
        this.laureat = laureat;
    }
}
